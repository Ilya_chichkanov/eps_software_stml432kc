#ifndef RFM69_H
#define RFM69_H
#include "stdint.h"
#include "rfm69_REG.h"                                                         // файл с регистрами приёмопередатчика
#include "rfm69_VAL.h"                                                         // файл со значениями регистров приёмопередатчика
/*==========================Блок определения  define==========================*/
#define RH_RF69_FXOSC 32000000.0                                               // Частота кварцевого генератора модуля RF69
#define RH_RF69_FSTEP  (RH_RF69_FXOSC / 524288)                                // Шаг синтезатора частоты step = RH_RF69_FXOSC / 2^19

/////////////////////////////////////////////////////////////////////////////////
//                    НЕОБХОДИМО ОТКАЛИБРОВАТЬ!!!!!!!                          //
//                                                                             //
#define RFM69_TEMPERATURE_0_CALIBRATION 55                                     // Значение регистра при 0 градусов Цельсия
/////////////////////////////////////////////////////////////////////////////////
 
/*=======================Конец блока определения  define======================*/


/*==================Блок определения  частных типов данных====================*/
 typedef enum
 {
   RFM_69_HIGH_POWER_OFF = 0,                                                   // Усилитель высокой мощности выключен
   RFM_69_HIGH_POWER_ON  = 1                                                    // Усилитель высокой мощности включён
 }rfm69_ishighpowermodule;                                                      // Статус усилителя высокой мощности
 
 typedef enum
 {
   NONE  = RH_RF69_DATAMODUL_MODULATIONSHAPING_FSK_NONE,                        // фильтр выключен
   BT1_0 = RH_RF69_DATAMODUL_MODULATIONSHAPING_FSK_BT1_0,                       // фильтр Гаусса, BT = 1.0
   BT0_5 = RH_RF69_DATAMODUL_MODULATIONSHAPING_FSK_BT0_5,                       // фильтр Гаусса, BT = 0.5
   BT0_3 = RH_RF69_DATAMODUL_MODULATIONSHAPING_FSK_BT0_3                        // фильтр Гаусса, BT = 0.3
 }rfm69_moduation_shaping;                                                      // Тип переменных для настройки фильтрации
 
 typedef enum {
  rfm69_1200bps=0,                                                                // 1 200 бит/с
  rfm69_2400bps,                                                                // 2 400 бит/с
  rfm69_4800bps,                                                                // 4 800 бит/с
  rfm69_9600bps,                                                                // 9 600 бит/с
  rfm69_19200bps,                                                               // 19 200 бит/с
  rfm69_38400bps,                                                               // 38 400 бит/с
  rfm69_57600bps,                                                               // 57 600 бит/с
  rfm69_115200bps,                                                              // 115 200 бит/с
  rfm69_250000bps,                                                              // 250 000 бит/с
  rfm69_300000bps                                                               // 300 000 бит/с
} rfm69_bitrate;                                                                // Тип переменных для установки скорости передачи данных
 
typedef enum {
  rfm69_RxBw_2_6 = 0,                                                               // 2.6 кГц
  rfm69_RxBw_3_1,                                                               // 3.1 кГц
  rfm69_RxBw_3_9,                                                               // 3.9 кГц
  rfm69_RxBw_5_2,                                                               // 5.2 кГц
  rfm69_RxBw_6_3,                                                               // 6.3 кГц
  rfm69_RxBw_7_8,                                                               // 7.8 кГц
  rfm69_RxBw_10_4,                                                              // 10.4 кГц
  rfm69_RxBw_12_5,                                                              // 12.5 кГц
  rfm69_RxBw_15_6,                                                              // 15.6 кГц
  rfm69_RxBw_20_8,                                                              // 20.8 кГц
  rfm69_RxBw_25_0,                                                              // 25.0 кГц
  rfm69_RxBw_31_3,                                                              // 31.3 кГц
  rfm69_RxBw_41_7,                                                              // 41.7 кГц
  rfm69_RxBw_50_0,                                                              // 50.0 кГц
  rfm69_RxBw_62_5,                                                              // 62.5 кГц
  rfm69_RxBw_83_3,                                                              // 83.3 кГц
  rfm69_RxBw_100_0,                                                             // 100.0 кГц
  rfm69_RxBw_125_0,                                                             // 125.0 кГц
  rfm69_RxBw_166_7,                                                             // 166.7 кГц
  rfm69_RxBw_200_0,                                                             // 200.0 кГц
  rfm69_RxBw_250_0,                                                             // 250.0 кГц
  rfm69_RxBw_333_3,                                                             // 333.3 кГц
  rfm69_RxBw_400_0,                                                             // 400.0 кГц
  rfm69_RxBw_500_0                                                              // 500.0 кГц
} rfm69_RxBw;                                                                   // Тип переменных для установки фильтра ширины пропускания приёмного канала
/*================Конец блока определения  частных типов данных===============*/
 
/*===================Блок определения функций библиотеки======================*/
uint8_t rfm69_LibInit(
                      void (*_rfm69_transfer)(uint16_t, uint16_t *),
                      void    (*_rfm69_CS_write_LOW)(void),
                      void    (*_rfm69_CS_write_HIGH)(void),
                      uint8_t (*_rfm69_MISO_read)(void),
                      uint8_t (*_rfm69_DIO0_read)(void),
                      uint8_t (*_rfm69_DIO1_read)(void),
                      uint8_t (*_rfm69_DIO2_read)(void),
                      uint8_t (*_rfm69_DIO3_read)(void),
                      uint8_t (*_rfm69_DIO4_read)(void),
                      uint8_t (*_rfm69_DIO5_read)(void),                  
                      void    (*_rfm69_delay_us)(uint32_t),
                      uint8_t _rfm69_packet_length,
                      void (*_rfm69_read_N_regs)(uint8_t, uint8_t *, uint8_t),
                      void (*_rfm69_write_N_regs)(uint8_t, uint8_t *, uint8_t),
                      uint64_t (*_rfm69_get_tick_us)(void)
                     );                                                        // Функция инициализации библиотеки
                      
void rfm69_init(uint8_t _rfm69_packet_length);                                 // Инициализирует приёмопередатчик с настройками по умолчанию
uint8_t rfm69_setModeIdle(void);                                               // Устанавливает  режим простоя приёмопередатчика
uint8_t rfm69_setModeRx(void);                                                 // Устанавливает режим работы приёмопередатчика на приём данных
uint8_t rfm69_setModeTx(void);                                                 // Устанавливает режим работы приёмопередатчика на передачу данных
void rfm69_sleep(void);                                                        // Выключает приёмопередатчик
void rfm69_reset(void);                                                        // Перезапускает приёмопередатчик
void rfm69_PowerLevel(int8_t power, rfm69_ishighpowermodule ishighpowermodule);// Устанавливает выходную мощность приёмопередатчика
void rfm69_SetFrequency(float freq);                                           // Устанавливает несущую частоту приёмопередатчика
void rfm69_Data_Modul(rfm69_moduation_shaping filter);                         // Настраивает режим работы с данными и настройки модуляции
void rfm69_setBitRate(rfm69_bitrate bit_rate);                                 // Установка скорости передачи данных
uint8_t rfm69_setFreqDeviation(float fdev);                                    // Устанавливает дивиацию несущей частоты примёмопередатчика
float rfm69_getFreqDeviation(void);                                            // Возвращает текущую дивиацию несущей частоты примёмопередатчика
void rfm69_set_filterBW(rfm69_RxBw BW, uint8_t dccFreqN);                      // Установка фильтра ширины пропускания приёмного канала
float rfm69_getReceiverBandWitdh(void);                                        // Возвращает значение фильтра ширины пропускания приёмного канала
void rfm69_packetFormat(uint16_t bytes, uint8_t syncSize, uint8_t syncTol);    // Настраивает формат пакета данных
uint8_t rfm69_setPayloadLength(uint8_t length);
uint8_t rfm69_send(uint8_t *dataForSend, uint8_t dataLength);                  // Отправляет данные по rfm69
uint8_t rfm69_receive(uint8_t *bufferForReceiveData, uint8_t bufferLength);    // Принимает данные по rfm69
uint8_t rfm69_check_can_send(void);
uint8_t rfm69_available(void);                                                 // Проверят доступен ли приём или нет
uint8_t rfm69_getLastRSSIReg(void); //последнее значение регистра RSSI
float rfm69_getFrequency(void);
float rfm69_getBitRate(void);                                                  // Возвращает скорость передачи данных, бит/с
int8_t rfm69_getTXPower_dBm(void);
int8_t rfm69_getTemperature(void);                                             // Возвращает температуру RFM69
void rfm69_read_reg(uint8_t adr, uint8_t *val);                                // Считывает значение регистра по SPI
void rfm69_write_reg(uint8_t adr, uint8_t val);
void rfm69_set_isTCXO(uint8_t isTCXO);
void rfm69_setSyncWords(uint8_t syncSize, uint8_t syncTol, uint8_t *syncWords);// Конфигурирует настройки синхрослов пакета
/*================Конец блока определения функций библиотеки==================*/
#endif
