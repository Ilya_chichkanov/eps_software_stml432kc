#ifndef YARILO_RADIO_CONFIG_H_
#define YARILO_RADIO_CONFIG_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
  float freqRX;//частота приёмника
  float freqTX;//частота передатчика
  uint32_t baudRateTx_id;//id скорости передачи данных
  uint32_t baudRateRx_id;//id скорости приёма данных
  float fdevRx;//Частота сдвига при приёме
  float fdevTx;//Частота сдвига при передаче
	uint32_t rxFilter_id;//id фильтра на приём
  uint32_t rxFilterDcc_id;//id настройки dc-canceller
  uint32_t modulation_tx;//режим модуляции при передаче - сейчас не реализовано на уровне драйвера RFM69
  uint32_t modulation_rx;//режим модуляции при приёме - сейчас не реализовано на уровне драйвера RFM69
  uint32_t modulation_filter_rx;//режим фильтра модуляции при приёме
  uint32_t modulation_filter_tx;//режим фильтра модуляции при приёме
  float dac_to_output_power_dBm_coef[4];//коэффициенты зависимости значения ЦАП от выходной мощности
  float txPower_W;//выходная мощность в дБм
  int32_t rfm69_output_power_dBm;//выходная мощность микросборки RFM69HW
  uint32_t txPower_DAC;//текущее значение выхода DAC
  uint32_t isBypass_power_amplifier_for_TX;//исключать ли усилитель мощности из тракта передачи
  uint32_t rf_packet_length;//длина единичного радиопакета
  uint32_t rf_preambule_length;//длина преамбулы
  uint32_t rf_sync_length;//длина синхрослова
  uint32_t sync_word_low;//первые 4 байта синхрослова
  uint32_t sync_word_high;//первые 4 байта синхрослова 
	uint32_t rf_allowable_bit_errors;//допустимое число ошибок в заголовке пакета
  uint32_t max_bus_frames_in_rf_packet;//максимальное число кадров шины в радиопакете
  uint32_t isUseCodding;//флаг использовать ли кодирование
  uint32_t bytesForCodding_NPAR;//число байт, используемых для кодирования
  uint32_t mcu_idle_freq_id;//id частоты МК в режим простоя
  uint32_t mcu_rxtx_freq_id;//id частоты МК в режиме приёма/передачи
  float wait_frames_for_send_delay;//период ожидания дополнительных пакетов для отправки
  float after_send_delay;//задержка после отправки пакета
  float radio_task_delay;//задержка в конце задачи радио
  float process_task_delay;//задержка в конце задачи обработки приёма
  float tmi_update_period;//период обновления внуренней телеметрии
  float monitor_period;//период работы задачи-монитора
  uint32_t fram_tmi_write_position;//положение лога тми в FRAM
  uint32_t time_save_period_s;//период обновления настроек в FRAM
  uint32_t tmi_save_to_fram_period_s;//период сохранения лога ТМИ в FRAM
  uint32_t tmi_send_period_s;//период отправки ТМИ
  uint32_t config_correct_status;//ключ
} yarilo_radio_config_fields_typedef;
  
#define YARILO_RADIO_CONFIG_LENGTH (sizeof(yarilo_radio_config_fields_typedef))

typedef union
{
  yarilo_radio_config_fields_typedef fields;
  uint8_t bytes[YARILO_RADIO_CONFIG_LENGTH];
} yarilo_radio_config;

void setDefaultCfg();
extern volatile yarilo_radio_config cfg_current;

#ifdef __cplusplus
}
#endif

#endif 
