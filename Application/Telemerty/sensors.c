#include "stm32l4xx_hal.h"

#include "sensors.h"
#include "board.h"
#include "main.h"
#include "stdio.h"
#include "status.h"
#include "log_utils.h"
#include "power.h"
#include "../../Application/Drivers/INA219.h"
#include "../../Application/Drivers/DS1631.h"
#include "../../Application/Drivers/PAC1934.h"
#include "../../Application/Drivers/RFM69_ex.h"
#include "../../Drivers/rfm69/rfm69.h"

extern UART_HandleTypeDef *muart;

void sensors_init() {
	struct status *status_dev = status_get();
    HAL_GPIO_WritePin(CH1_DIS_GPIO_Port, CH1_DIS_Pin, GPIO_PIN_SET); //Проверка работы релле
    HAL_GPIO_WritePin(EN3_GPIO_Port, EN3_Pin, GPIO_PIN_SET); //Канал 3 не включать
    HAL_GPIO_WritePin(EN1_GPIO_Port, EN1_Pin|EN2_Pin|EN4_Pin, GPIO_PIN_RESET); //Остальные включить
    status_dev->sens_err[SENS_EPS_INA219_CHRG1] = INA219_SetCalibration(&I2C_EPS,INA219_CHRG1);
    status_dev->sens_err[SENS_EPS_INA219_CHRG2] = INA219_SetCalibration(&I2C_EPS,INA219_CHRG2);
    status_dev->sens_err[SENS_EPS_INA219_VBAT3] = INA219_SetCalibration(&I2C_EPS,INA219_VBAT3);
    status_dev->sens_err[SENS_EPS_INA219_BATP] = INA219_SetCalibration(&I2C_EPS,INA219_BATP);
    status_dev->sens_err[SENS_EPS_INA219_VBAT2] = INA219_SetCalibration(&I2C_EPS,INA219_VBAT2);
    status_dev->sens_err[SENS_EPS_INA219_BATC1] = INA219_SetCalibration(&I2C_EPS,INA219_BATC1);
    status_dev->sens_err[SENS_EPS_INA219_BATC2] = INA219_SetCalibration(&I2C_EPS,INA219_BATC2);

    status_dev->sens_err[SENS_EPC_DS1631_BAT1] = DS1631_Init(DS1631_BAT1,&I2C_EPS)||
    									  	  	 DS1631_SetTH(DS1631_BAT1, DS1631_TH)||
												 DS1631_SetTL(DS1631_BAT1, DS1631_TL);

    status_dev->sens_err[SENS_EPC_DS1631_BAT2] = DS1631_Init(DS1631_BAT2,&I2C_EPS)||
    									  	  	 DS1631_SetTH(DS1631_BAT2, DS1631_TH)||
												 DS1631_SetTL(DS1631_BAT2, DS1631_TL);
    uint8_t pac1934init_res = PAC1934_Init(&I2C_EPS);
    status_dev->sens_err[SENS_EPS_PAC1934_CH1] = pac1934init_res;
    status_dev->sens_err[SENS_EPS_PAC1934_CH2] = pac1934init_res;
    status_dev->sens_err[SENS_EPS_PAC1934_CH3] = pac1934init_res;
    status_dev->sens_err[SENS_EPS_PAC1934_CH4] = pac1934init_res;
    PAC1934_Refresh(&I2C_EPS);
    PAC1934_RefreshV(&I2C_EPS);
    power_sw_on(1);//switch obc on
    power_sw_on(2);//switch uhf on
    power_sw_on(3);//switch rfm on
    DEBUG_LED_ON;
    HAL_GPIO_WritePin(CAN2_SELECT_GPIO_Port, CAN2_SELECT_Pin, GPIO_PIN_SET);

    for (uint8_t i = 0; i < SENS_EPC_COUNT-1; i++) {
        if (status_dev->sens_err[i] != 0) {
            char str_send[30];
            sprintf(str_send, "ERROR WHILE INIT SENSOR %d\r\n", i);
            HAL_UART_Transmit(muart, (uint8_t *) str_send, strlen((char *) str_send), 100);
        }
    }

    i2c_Scan(&hi2c1);
	setDefaultCfg();
	HAL_Delay(20);//Delay for power supply rfm69
	rfm69_LibInit(
				 &UM_SPI1_transfer,
				 &rfm69_CS_set_LOW,
				 &rfm69_CS_set_HIGH,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 &um_delay_us,
				 cfg_current.fields.rf_packet_length,
				 0,
				 0,
				 &rfm69_get_tick_us
			   );
	updateRfm69Cfg();

}
