/*
 * telemerty.c
 *
 *  Created on: 17 апр. 2021 г.
 *      Author: Ilia
 */
#include  "telemerty.h"
#include "log_utils.h"
#include "status.h"
#include "errors.h"
#include "../../Application/Drivers/INA219.h"
#include "../../Application/Drivers/DS1631.h"
#include "../../Application/Drivers/PAC1934.h"
static const char * tag = "telemerty";

static struct telemerty_reg tel_reg;

void telemerty_regylar(){
	uint16_t error_id;
	PAC1934_RefreshV(&I2C_EPS);
	PAC1934_Refresh(&I2C_EPS);
	struct status* dev_status = status_get();
	if(dev_status->sens_err[SENS_EPS_INA219_CHRG1]==0){
	  error_id = INA219_GetUI(&I2C_EPS,INA219_CHRG1, &tel_reg.voltage_param[CHRG1 - UI_SENSORS_FIRST], &tel_reg.current_param[CHRG1 - UI_SENSORS_FIRST]);
	  dev_status->sens_err[SENS_EPS_INA219_CHRG1] = error_id==0? 0: ERR_INA219_CHRG1;
	}
	if(dev_status->sens_err[SENS_EPS_INA219_CHRG2]==0){
	  error_id = INA219_GetUI(&I2C_EPS,INA219_CHRG2, &tel_reg.voltage_param[CHRG2 - UI_SENSORS_FIRST], &tel_reg.current_param[CHRG2 - UI_SENSORS_FIRST]);
	  dev_status->sens_err[SENS_EPS_INA219_CHRG2] = (error_id==0)? 0: ERR_INA219_CHRG2;
	}
	if(dev_status->sens_err[SENS_EPS_INA219_VBAT3]==0){
	  error_id = INA219_GetUI(&I2C_EPS, INA219_VBAT3, &tel_reg.voltage_param[VBAT3 - UI_SENSORS_FIRST], &tel_reg.current_param[VBAT3 - UI_SENSORS_FIRST]);
	  dev_status->sens_err[SENS_EPS_INA219_VBAT3] = (error_id==0)? 0: ERR_INA219_VBAT3;
	}

	if(dev_status->sens_err[SENS_EPS_INA219_BATP]==0){
	  error_id = INA219_GetUI(&I2C_EPS, INA219_BATP, &tel_reg.voltage_param[BATP - UI_SENSORS_FIRST], &tel_reg.current_param[BATP - UI_SENSORS_FIRST]);
	  dev_status->sens_err[SENS_EPS_INA219_BATP] = (error_id==0)? 0: ERR_INA219_BATP;
	}

	if(dev_status->sens_err[SENS_EPS_INA219_VBAT2]==0){
	  error_id = INA219_GetUI(&I2C_EPS, INA219_VBAT2, &tel_reg.voltage_param[VBAT2 - UI_SENSORS_FIRST], &tel_reg.current_param[VBAT2 - UI_SENSORS_FIRST]);
	  dev_status->sens_err[SENS_EPS_INA219_VBAT2] = (error_id==0)? 0: ERR_INA219_VBAT2;
	}

	if(dev_status->sens_err[SENS_EPS_INA219_BATC1]==0){
	  error_id = INA219_GetUI(&I2C_EPS, INA219_BATC1, &tel_reg.voltage_param[BATC1 - UI_SENSORS_FIRST], &tel_reg.current_param[BATC1 - UI_SENSORS_FIRST]);
	  dev_status->sens_err[SENS_EPS_INA219_BATC1] = (error_id==0)? 0: ERR_INA219_BATC1;
	}

	if(dev_status->sens_err[SENS_EPS_INA219_BATC2]==0){
	  error_id = INA219_GetUI(&I2C_EPS,INA219_BATC2, &tel_reg.voltage_param[BATC2 - UI_SENSORS_FIRST], &tel_reg.current_param[BATC2 - UI_SENSORS_FIRST]);
	  dev_status->sens_err[SENS_EPS_INA219_BATC2] = (error_id==0)? 0: ERR_INA219_BATC2;
	}

	if(dev_status->sens_err[SENS_EPS_PAC1934_CH1]==0){
	  error_id = PAC1934_GetUI_AVG(&I2C_EPS, PAC1934_PCH1, &tel_reg.voltage_param[PCH1 - UI_SENSORS_FIRST], &tel_reg.current_param[PCH1 - UI_SENSORS_FIRST]);
	  dev_status->sens_err[SENS_EPS_PAC1934_CH1] = (error_id==0)? 0: ERR_INA219_PCH1;
	}
	if(dev_status->sens_err[SENS_EPS_PAC1934_CH2]==0){
	  error_id =  PAC1934_GetUI_AVG(&I2C_EPS,PAC1934_PCH2, &tel_reg.voltage_param[PCH2 - UI_SENSORS_FIRST], &tel_reg.current_param[PCH2 - UI_SENSORS_FIRST]);
	  dev_status->sens_err[SENS_EPS_PAC1934_CH2] = (error_id==0)? 0: ERR_INA219_PCH2;
	}
	if(dev_status->sens_err[SENS_EPS_PAC1934_CH3]==0){
	  error_id =  PAC1934_GetUI_AVG(&I2C_EPS, PAC1934_PCH3, &tel_reg.voltage_param[PCH3 - UI_SENSORS_FIRST], &tel_reg.current_param[PCH3 - UI_SENSORS_FIRST]);
	  dev_status->sens_err[SENS_EPS_PAC1934_CH3] = (error_id==0)? 0: ERR_INA219_PCH3;
	}
	if(dev_status->sens_err[SENS_EPS_PAC1934_CH4]==0){
	  error_id =  PAC1934_GetUI_AVG(&I2C_EPS,PAC1934_PCH3, &tel_reg.voltage_param[PCH3 - UI_SENSORS_FIRST], &tel_reg.current_param[PCH3 - UI_SENSORS_FIRST]);
	  dev_status->sens_err[SENS_EPS_PAC1934_CH4] = (error_id==0)? 0: ERR_INA219_PCH4;
	}
	if(dev_status->sens_err[SENS_EPC_DS1631_BAT1]==0){
	  error_id = DS1631_GetTemperature(DS1631_BAT1,&tel_reg.temperature_param[T_SENS_BAT1 - T_SENS_FIRST]);
	  dev_status->sens_err[SENS_EPC_DS1631_BAT1] = (error_id==0)? 0: ERR_DS1631_GETTING;
	}
	if(dev_status->sens_err[SENS_EPC_DS1631_BAT2]==0){
	  error_id = DS1631_GetTemperature(DS1631_BAT2,&tel_reg.temperature_param[T_SENS_BAT2 - T_SENS_FIRST]);
	  dev_status->sens_err[SENS_EPC_DS1631_BAT2] = (error_id==0)? 0: ERR_DS1631_GETTING;
	}
	time_get(&tel_reg.time);
	LOGI(tag, "CurrenT: ");
	LOG_int16_array(tel_reg.current_param,11);
	LOGI(tag, "Voltage: ");
	LOG_int16_array(tel_reg.voltage_param,11);
	LOGI(tag, "Temperature: ");
	LOG_int16_array(tel_reg.temperature_param,2);

	for (uint8_t i=0;i<SENS_EPC_COUNT;i++){
	  if(dev_status->sens_err[i]!=0){
		  LOGI(tag, "ERRORS IN SENSORS");
		  LOG_uint8_array(dev_status->sens_err, SENS_EPC_COUNT);
		  break;
	  }
	  if(i==SENS_EPC_COUNT-1){
		  LOGI(tag, "ALL SENSORS ARE OK");
	  }
	}
}

struct telemerty_reg * telemerty_regular_get(){
	return &tel_reg;
}
