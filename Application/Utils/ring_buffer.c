
#include "ring_buffer.h"
#include "stdint.h"


uint16_t RING_CRC16ccitt(const RING_buffer_t *buf, uint16_t lenght, uint16_t position)
{
	return RING_CRC16ccitt_Intermediate( buf, lenght, 0xFFFF, position);
}


uint16_t RING_CRC16ccitt_Intermediate(const RING_buffer_t *buf, uint16_t lenght, uint16_t tmpCrc, uint16_t position)
{
	uint16_t crc = tmpCrc;
	uint16_t crctab;
	uint8_t byte;

    while (lenght--)
    {
        crctab = 0x0000;
        byte = (RING_ShowSymbol(buf, lenght + position))^( crc >> 8 );
        if( byte & 0x01 ) crctab ^= 0x1021;
        if( byte & 0x02 ) crctab ^= 0x2042;
        if( byte & 0x04 ) crctab ^= 0x4084;
        if( byte & 0x08 ) crctab ^= 0x8108;
        if( byte & 0x10 ) crctab ^= 0x1231;
        if( byte & 0x20 ) crctab ^= 0x2462;
        if( byte & 0x40 ) crctab ^= 0x48C4;
        if( byte & 0x80 ) crctab ^= 0x9188;

        crc = ( ( (crc & 0xFF)^(crctab >> 8) ) << 8 ) | ( crctab & 0xFF );
    }
    return crc;
}

uint16_t crc16_X_Modern(const RING_buffer_t *buf, uint16_t lenght, uint16_t position)
{
	uint8_t byte,xor;
	uint16_t crc = 0x0000;
	uint16_t poly = 0x1021;
	uint8_t mask[8] = {0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01}; // # set of possible currentbit values
    for(int jj=0;jj<lenght;jj++)
	{
    	byte = (RING_ShowSymbol(buf, jj + position));
    	for(int i=0;i<8;i++){
    		 if (crc & 0x8000){
    		    xor = 1;
    		 }
    		 else{
    			xor = 0;
    		 }
    		 crc=crc<<1;
             if (byte & mask[i] ){
                 crc+=1;
             }
             if (xor){
                 crc^=poly;
             }
    	}
	}
    for (int i=0;i<16;i++){
        if (crc & 0x8000){
            xor = 1;
        }
        else{
            xor = 0;
        }
        crc=crc<<1;
        if (xor){
            crc ^= poly;
        }
    }
    crc = crc & 0xFFFF;
    return crc;
}

RING_ErrorStatus_t RING_Init(RING_buffer_t *ring, uint8_t *buf, uint16_t size)
{
    ring->size = size;
    ring->buffer = buf;
    RING_Clear( ring );

    return ( ring->buffer ? RING_SUCCESS : RING_ERROR ) ;
}


uint16_t RING_GetCount(const RING_buffer_t *buf)
{
    uint16_t retval = 0;
    if (buf->idxIn < buf->idxOut) retval = buf->size + buf->idxIn - buf->idxOut;
    else retval = buf->idxIn - buf->idxOut;
    return retval;
}

void RING_Clear(RING_buffer_t* buf)
{
    buf->idxIn = 0;
    buf->idxOut = 0;
}


void RING_Put( RING_buffer_t* buf, uint8_t symbol)
{
    buf->buffer[buf->idxIn++] = symbol;
    if (buf->idxIn >= buf->size) buf->idxIn = 0;
}

void RING_Put16( RING_buffer_t* buf, uint16_t symbol)
{
/*    buf->buffer[buf->idxIn++] = symbol >> 8;
    if (buf->idxIn >= buf->size) buf->idxIn = 0;
    buf->buffer[buf->idxIn++] = symbol & 0xFF;
    if (buf->idxIn >= buf->size) buf->idxIn = 0;*/
    buf->buffer[buf->idxIn++] = (uint8_t)(symbol >>  0);
    if (buf->idxIn >= buf->size) buf->idxIn = 0;
    buf->buffer[buf->idxIn++] = (uint8_t)(symbol >>  8);
    if (buf->idxIn >= buf->size) buf->idxIn = 0;
}

void RING_Put32( RING_buffer_t* buf, uint32_t symbol)
{
    buf->buffer[buf->idxIn++] = (uint8_t)(symbol >>  0);
    if (buf->idxIn >= buf->size) buf->idxIn = 0;
    buf->buffer[buf->idxIn++] = (uint8_t)(symbol >>  8);
    if (buf->idxIn >= buf->size) buf->idxIn = 0;
    buf->buffer[buf->idxIn++] = (uint8_t)(symbol >>  16);
    if (buf->idxIn >= buf->size) buf->idxIn = 0;
    buf->buffer[buf->idxIn++] = (uint8_t)(symbol >>  24);
    if (buf->idxIn >= buf->size) buf->idxIn = 0;
}

void RING_PutBuffr(RING_buffer_t *ringbuf, const uint8_t *src, uint16_t len)
{
    while(len--) RING_Put(ringbuf, *(src++));
}


uint8_t RING_Pop(RING_buffer_t *buf)
{
    uint8_t retval = buf->buffer[buf->idxOut++];
    if (buf->idxOut >= buf->size) buf->idxOut = 0;
    return retval;
}

uint16_t RING_Pop16(RING_buffer_t *buf)
{
    uint16_t retval = RING_Pop(buf) << 8;
    retval += RING_Pop(buf);
    return retval;
}


uint32_t RING_Pop32(RING_buffer_t *buf)
{
/*    uint32_t retval  = RING_Pop(buf) << 8;
    retval += RING_Pop(buf) << 8;
    retval += RING_Pop(buf) << 8;
    retval += RING_Pop(buf);*/
    uint32_t retval  = RING_Pop(buf);
	retval |= (RING_Pop(buf) << 8);
	retval |= (RING_Pop(buf) << 16);
	retval |= (RING_Pop(buf) << 24);
/*	uint8_t data[4];
	RING_PopBuffr(buf,data,4);
	uint32_t retval = data[0] | (data[1] << 8) | (data[2] << 16) | (data[3] << 24);*/
    return retval;
}


void RING_PopBuffr(RING_buffer_t *ringbuf, uint8_t *destination, uint16_t len)
{
    while(len--) *(destination++) = RING_Pop(ringbuf);
}


void RING_PopString(RING_buffer_t *ringbuf, char *string)
{
    while(RING_ShowSymbol(ringbuf,0) > 0) *(string++) = RING_Pop(ringbuf);
}


int32_t RING_ShowSymbol(const RING_buffer_t *buf, uint16_t symbolNumber)
{
    uint32_t pointer = buf->idxOut + symbolNumber;
    int32_t  retval = -1;
    if (symbolNumber < RING_GetCount(buf))
    {
        if (pointer > buf->size) pointer -= buf->size;
        retval = buf->buffer[ pointer ] ;
    }
    return retval;
}
