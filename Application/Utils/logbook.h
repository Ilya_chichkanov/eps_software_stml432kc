/*
 * log_book.h
 *
 *  Created on: 27 ���. 2021 �.
 *      Author: Ilia
 */

#ifndef UTILS_LOGBOOK_H_
#define UTILS_LOGBOOK_H_

#include "stm32l4xx_hal.h"
#include "../Drivers/flash.h"
#include "default_settings.h"
#include "time.h"
#pragma push(pack)
#pragma pack(1)
struct Logbook_t{
	uint32_t record_num;     //4 byte
	uint16_t error;         // 2 byte
	struct time_board time; // 4 byte
	uint16_t param1;        // 2 byte
	uint16_t param2;        // 2 byte
	uint16_t param3;        // 2 byte
};
#pragma pop(pack)

/*

struct Logbook_t{
	uint32_t record_num;     //4 byte
	uint16_t error;         // 1 byte
	uint16_t param1;        // 2 byte
	uint16_t param2;        // 2 byte
	uint16_t param3;        // 2 byte
	uint32_t param4;        // 4 byte
};
*/

typedef enum
{
  LOG_OK        =  0x00,
  LOG_ERROR     =  0x01,
  LOG_EMPTY     =  0x02,
  LOG_FULL      =  0x03
} LOG_StatusTypeDef;

#define LOGBOOK_DWORD_CNT sizeof(struct Logbook_t)/ sizeof(uint64_t)
#define LOGBOOK_WORD_CNT  sizeof(struct Logbook_t)/ sizeof(uint32_t)
#define LOOGBOOK_ADDRESS_START FLASH_LOGBOOK_ADDRESS
#define LOOGBOOK_ADDRESS_END   LOOGBOOK_ADDRESS_START+FLASH_PAGE_SIZE

uint16_t logbook_free(void);
uint16_t logbook_write(struct Logbook_t* logbook);
uint16_t logbook_readlast(struct Logbook_t* logbook);
uint16_t logbook_read(struct Logbook_t* logbook, uint16_t record_num);
uint32_t logbook_get_size(void);
void     logbook_init(void);
#endif /* UTILS_LOGBOOK_H_ */
