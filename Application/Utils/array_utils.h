/*
 * array_utilc.h
 *
 *  Created on: 4 апр. 2021 г.
 *      Author: Ilia
 */

#ifndef UTILS_ARRAY_UTILS_H_
#define UTILS_ARRAY_UTILS_H_
#include "stm32l4xx_hal.h"
uint32_t array_pop32(uint8_t *data);
uint16_t array_pop16(uint8_t *data);
#endif /* UTILS_ARRAY_UTILS_H_ */
