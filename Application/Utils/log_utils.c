/*
 * log_utils.h
 *
 *  Created on: 19 ���. 2021 �.
 *      Author: Ilia
 */

#include "log_utils.h"
#include "stdio.h"
static UART_HandleTypeDef * uart;
void (*log_function)(const char *tag, const char *msg);
static void log_print(const char *tag, const char *msg);
SemaphoreHandle_t log_mtx;
StaticSemaphore_t log_mtx_buffer;

static void log_print(const char *tag, const char *msg)
{
	HAL_UART_Transmit(uart, tag, strlen(tag), 1000);
	HAL_UART_Transmit(uart, ":  ", 3, 1000);
	HAL_UART_Transmit(uart, msg, strlen(msg), 1000);
	HAL_UART_Transmit(uart, "\n\r", 2, 1000);
}


void log_utils_init(UART_HandleTypeDef * uart_){
	uart = uart_;
	log_mtx = xSemaphoreCreateMutexStatic(&log_mtx_buffer);
	xSemaphoreGive(log_mtx);
	log_function = log_print;
}

void LOGI(const char* tag, const char *msg)
{
	xSemaphoreTake(log_mtx, portMAX_DELAY);
	log_function(tag, msg);
	xSemaphoreGive(log_mtx);
}

void LOG_float_array(const float* array,uint8_t n)//allow to send float 3 dim array  and string into UART
{
	xSemaphoreTake(log_mtx, portMAX_DELAY);
	for (uint8_t i=0;i<n;i++ ){
		char str_arr[40]="";
		if (array[i]<0.0){
			float tmp = -array[i];
			sprintf(str_arr, "-%d.%03d ", (uint16_t)tmp, (uint16_t)((tmp - (uint16_t)tmp)*1000.) ); //put float into string
		}
		else{
			sprintf(str_arr, "%d.%03d ", (uint16_t)array[i], (uint16_t)((array[i] - (uint16_t)array[i])*1000.) ); //put float into string
		}
		if  (i==n-1){
			sprintf(str_arr,"%s\r\n", str_arr);
		}
		HAL_UART_Transmit(uart,(uint8_t*)str_arr, strlen((char *)str_arr), 100);
	}
	xSemaphoreGive(log_mtx);
}

void LOG_int16_array(const int16_t *array,uint8_t n)//allow to send float 3 dim array  and string into UART
{
	xSemaphoreTake(log_mtx, portMAX_DELAY);
	for (uint8_t i=0;i<n;i++ ){
		char str_arr[40];
		sprintf(str_arr, "%d ",array[i]);
		if  (i==n-1){
			sprintf(str_arr,"%s\r\n", str_arr);
		}
		HAL_UART_Transmit(uart,(uint8_t*)str_arr, strlen((char *)str_arr), 100);
	}
	xSemaphoreGive(log_mtx);
}
void LOG_uint8_array(const uint8_t *array,uint8_t n)//allow to send float 3 dim array  and string into UART
{
	xSemaphoreTake(log_mtx, portMAX_DELAY);
	for (uint8_t i=0;i<n;i++ ){
		char str_arr[40];
		sprintf(str_arr, "%d ",array[i]);
		if  (i==n-1){
			sprintf(str_arr,"%s\r\n", str_arr);
		}
		HAL_UART_Transmit(uart,(uint8_t*)str_arr, strlen((char *)str_arr), 100);
	}
	xSemaphoreGive(log_mtx);
}
void i2c_Scan(I2C_HandleTypeDef *hi2c){
    char uart2Data[24] = "Connected to UART\r\n";
    char str [255] = "";

    HAL_UART_Transmit(uart, (uint8_t *)&uart2Data,sizeof(uart2Data), 0xFFFF);

   	sprintf(str, "%s\r\n",str);
   	HAL_UART_Transmit(uart, (uint8_t *)str,strlen(str), 0xFFFF);
   	sprintf(str, "Scanning I2C bus:\r\n");
   	HAL_UART_Transmit(uart, (uint8_t *)str,strlen(str), 0xFFFF);
  	HAL_StatusTypeDef result;
   	uint8_t i= 0;
	for (i=1; i<128; i++)
   	{
   	  result = HAL_I2C_IsDeviceReady(hi2c, (uint16_t)(i<<1), 2, 2);
   	  if (result != HAL_OK) // HAL_ERROR or HAL_BUSY or HAL_TIMEOUT
   	  {
   		  sprintf(str, "."); // No ACK received at that address
   		  HAL_UART_Transmit(uart, (uint8_t *)str,strlen(str), 0xFFFF);
   	  }
   	  if (result == HAL_OK)
   	  {
   		  sprintf(str, "0x%X", i); // Received an ACK at that address
   		  HAL_UART_Transmit(uart, (uint8_t *)str,strlen(str), 0xFFFF);
   	  }
   	}
   	sprintf(str, "\r\n");
   	HAL_UART_Transmit(uart, (uint8_t *)str,strlen(str), 0xFFFF);

}
