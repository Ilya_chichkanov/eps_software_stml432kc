/*
 * board.h
 *
 *  Created on: 29 ���. 2021 �.
 *      Author: Ilia
 */

#ifndef SETTINGS_BOARD_H_
#define SETTINGS_BOARD_H_
#include "stm32l4xx_hal.h"

#define i2c_INA219  hi2c1
#define i2c_PAC1934 hi2c1
#define i2c_LSM9DS1 hi2c1
#define i2c_SATBUS  hi2c2
#define I2C_EPS     hi2c1 //EPS_EQM


#define UI_SENSORS_FIRST 0U
#define UI_SENSORS_OBC_FIRST 0U

#define BB_VCC 		0U
#define COIL_VCC 	1U
#define OBC_3V3		2U
#define COIL_X		3U
#define COIL_Y		4U
#define COIL_Z		5U

#define UI_SENSORS_OBC_LAST 5U
#define UI_SENSORS_EPS_FIRST 6U

#define BATC1		6U
#define CHRG1		7U
#define BATC2		8U
#define VBAT2		9U
#define VBAT3		10U
#define BATP		11U
#define PCH1		12U
#define PCH2		13U
#define PCH3		14U
#define PCH4		15U
#define CHRG2		16U

#define UI_SENSORS_EPS_LAST 16U
#define UI_SENSORS_LAST 16U

//IMU
#define IMU_PARAM_FIRST	20U

#define MAGN_X		20U
#define MAGN_Y		21U
#define MAGN_Z		22U
#define GYRO_X		23U
#define GYRO_Y		24U
#define GYRO_Z		25U
#define ASCIL_X		26U
#define ASCIL_Y		27U
#define ASCIL_Z		28U
#define UNC_MX		29U
#define UNC_MY		30U
#define UNC_MZ		31U
#define PWM_X		32U
#define PWM_Y		33U
#define PWM_Z		34U
#define CURRANGEL	35U

#define IMU_PARAM_LAST	35U

//temperature

#define T_SENS_FIRST 		36U
#define T_SENS_OBC_FIRST 	36U

#define T_SENS_COILXY 		36U
#define T_SENS_COILZ		37U
#define T_SENS_IMU	 		38U
#define T_SENS_SW_3V3		39U

#define T_SENS_OBC_LAST 	39U
#define T_SENS_EPS_FIRST 	40U

#define T_SENS_BAT1			40U
#define T_SENS_BAT2			41U

#define T_SENS_EPS_LAST		41U
#define T_SENS_LAST 		41U

#define PARAM_LAST			41U



/*OBC SENS*/
#define SENS_OBC_COUNT		           23

#define SENS_OBC_TERM100NA_COILZ       0
#define SENS_OBC_TERM100NA_COILXY      1
#define SENS_OBC_TERM100NA_IMU         2
#define SENS_OBC_TERM100NA_SW_3V3      3
#define SENS_OBC_INA219_BB_VCC         4
#define SENS_OBC_INA219_COIL_VCC       5
#define SENS_OBC_INA219_OBC_3V3        6
#define SENS_OBC_INA219_CHRG1          7
#define SENS_OBC_INA219_CHRG2          8
#define SENS_OBC_INA219_VBAT3          9
#define SENS_OBC_INA219_BATP           10
#define SENS_OBC_INA219_VBAT2          11
#define SENS_OBC_INA219_BATC1          12
#define SENS_OBC_INA219_BATC2          13
#define SENS_OBC_LSM9DS1               14
#define SENS_OBC_PAC1934_COL_X         15
#define SENS_OBC_PAC1934_COL_Y         16
#define SENS_OBC_PAC1934_COL_Z         17
#define SENS_OBC_PAC1934_CH1           18
#define SENS_OBC_PAC1934_CH2           19
#define SENS_OBC_PAC1934_CH3           20
#define SENS_OBC_PAC1934_CH4           21
#define SENS_OBC_SD_CARD               22

/*EPC SENS*/
#define SENS_EPC_COUNT		           13
#define SENS_EPS_INA219_CHRG1          0
#define SENS_EPS_INA219_CHRG2          1
#define SENS_EPS_INA219_VBAT3          2
#define SENS_EPS_INA219_BATP           3
#define SENS_EPS_INA219_VBAT2          4
#define SENS_EPS_INA219_BATC1          5
#define SENS_EPS_INA219_BATC2          6
#define SENS_EPS_PAC1934_CH1           7
#define SENS_EPS_PAC1934_CH2           8
#define SENS_EPS_PAC1934_CH3           9
#define SENS_EPS_PAC1934_CH4           10
#define SENS_EPC_DS1631_BAT1           11
#define SENS_EPC_DS1631_BAT2           12
#endif /* SETTINGS_BOARD_H_ */
