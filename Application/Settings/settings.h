#ifndef SETTINGS_SETTINGS_H_
#define SETTINGS_SETTINGS_H_

#include "default_settings.h"
#include "stm32l4xx_hal.h"

#pragma push(pack)
#pragma pack(1)

struct settings {
    uint8_t can_device_id;
    uint8_t fw_version;
    // device info
    uint32_t tm_reg_period;
    char dev_serial[DEV_SERIAL_LEN];
};
#pragma pop(pack)

#define SETTINGS_WORD_CNT  sizeof(struct settings)/ sizeof(uint32_t)

void settings_load_default();
struct settings* settings_get();
uint16_t settings_store_to_flash();
uint16_t setttings_read_from_flash();

#endif /* SETTINGS_SETTINGS_H_ */
