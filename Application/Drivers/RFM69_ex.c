/*
 * RFM69.c
 *
 *  Created on: Sep 18, 2020
 *      Author: Данил
 */

#include "main.h"
#include "spi.h"
#include "usart.h"
#include "RFM69_ex.h"


void rfm69_CS_set_LOW(void)
{
	HAL_GPIO_WritePin(SPI1_NSS_GPIO_Port, SPI1_NSS_Pin, GPIO_PIN_RESET);
  //digitalWriteMDR(MDR_PORTF, PORT_Pin_2, LOW);
}

void rfm69_CS_set_HIGH(void)
{
	HAL_GPIO_WritePin(SPI1_NSS_GPIO_Port, SPI1_NSS_Pin, GPIO_PIN_SET);
  //digitalWriteMDR(MDR_PORTF, PORT_Pin_2, HIGH);
}

void rfm69_reset_hard(void)
{
	HAL_GPIO_WritePin(RF_RESET_GPIO_Port, RF_RESET_Pin, GPIO_PIN_SET);
  //digitalWriteMDR(MDR_PORTA, PORT_Pin_4, HIGH);//RESET FM69
	HAL_Delay(1);
  //um_delay_us(150UL);//100 us
  HAL_GPIO_WritePin(RF_RESET_GPIO_Port, RF_RESET_Pin, GPIO_PIN_RESET);
  //digitalWriteMDR(MDR_PORTA, PORT_Pin_4, LOW);//RESET FM69
  HAL_Delay(10);
  //um_delay_us(6000UL);//5000 us
}

void UM_SPI1_transfer(uint16_t write, uint16_t *read)
{
	HAL_SPI_TransmitReceive(&hspi1, (uint8_t *)&write, (uint8_t *)read, 1, 100);
}

void um_delay_us(uint32_t delay)
{
	if(delay < 1000)
		delay = 1000;
	delay = delay / 1000;
	HAL_Delay(delay);
}

uint64_t rfm69_get_tick_us(void)
{
	return HAL_GetTick() * 1000;
}

void updateRfm69Cfg()
{
  rfm69_reset_hard();
  um_delay_us(200000);

  rfm69_setModeIdle();

#ifdef RFM69_ROSINKA
  rfm69_set_isTCXO(1);//включение TCXO - на практике похоже нет разницы, включено �?то или нет
#endif

	rfm69_SetFrequency(cfg_current.fields.freqRX);
	if(cfg_current.fields.baudRateRx_id <= (uint32_t)rfm69_300000bps)
	{
		rfm69_setBitRate((rfm69_bitrate)cfg_current.fields.baudRateRx_id);
		rfm69_setFreqDeviation(cfg_current.fields.fdevRx);
		rfm69_set_filterBW((rfm69_RxBw)cfg_current.fields.rxFilter_id, cfg_current.fields.rxFilterDcc_id);
	}
	else
	{
		rfm69_setBitRate(rfm69_9600bps);
		rfm69_setFreqDeviation(19200.0);
		rfm69_set_filterBW(rfm69_RxBw_25_0, 6);
		cfg_current.fields.baudRateRx_id = (uint32_t)rfm69_9600bps;
		cfg_current.fields.rxFilter_id = (uint32_t)rfm69_RxBw_25_0;
		cfg_current.fields.rxFilterDcc_id = 6;
	}

	rfm69_Data_Modul(cfg_current.fields.modulation_filter_rx);
#ifdef RFM69_ROSINKA
	rfm69_PowerLevel(cfg_current.fields.rfm69_output_power_dBm, RFM_69_HIGH_POWER_OFF);
#else
  rfm69_PowerLevel(cfg_current.fields.rfm69_output_power_dBm, RFM_69_HIGH_POWER_ON);
#endif
	//cfg_current.fields.txPower_DAC = calcDACforPower(cfg_current.fields.txPower_W);
	rfm69_packetFormat(cfg_current.fields.rf_preambule_length,
	                   cfg_current.fields.rf_sync_length,
	                   cfg_current.fields.rf_allowable_bit_errors);
	uint8_t syncWord[8];
	syncWord[0] = (uint8_t)cfg_current.fields.sync_word_low;
	syncWord[1] = (uint8_t)(cfg_current.fields.sync_word_low >> 8);
	syncWord[2] = (uint8_t)(cfg_current.fields.sync_word_low >> 16);
	syncWord[3] = (uint8_t)(cfg_current.fields.sync_word_low >> 24);
	syncWord[4] = (uint8_t)cfg_current.fields.sync_word_high;
	syncWord[5] = (uint8_t)(cfg_current.fields.sync_word_high >> 8);
	syncWord[6] = (uint8_t)(cfg_current.fields.sync_word_high >> 16);
	syncWord[7] = (uint8_t)(cfg_current.fields.sync_word_high >> 24);
	rfm69_setSyncWords(cfg_current.fields.rf_sync_length, cfg_current.fields.rf_allowable_bit_errors, syncWord);
	rfm69_setPayloadLength(cfg_current.fields.rf_packet_length);
	rfm69_write_reg(RH_RF69_REG_58_TESTLNA, 0x2D);                                //режим повышенной чув�?твительно�?ти
	rfm69_write_reg(RH_RF69_REG_18_LNA, 0x00);                                    //Zin дл�? LNA = 50 Ом - нужно проверить, даёт ли �?то увеличение чув�?твительно�?ти
	rfm69_write_reg(0x28, 0x10);//очи�?тка FIFO
	rfm69_write_reg(0x3D, 0x02);//включаем autoRxRestart
	rfm69_write_reg(RH_RF69_REG_25_DIOMAPPING1,
				  RH_RF69_DIOMAPPING1_DIO0MAPPING_01);                          // У�?танавливаем линию прерывани�? 0 в режим готовно�?ти прин�?ти�? данных
	rfm69_write_reg(0x13, 0x00);//отключение overcurent protection
	//rfm69_write_reg(0x0B, 0x20);//improved AFC for low modulation index - похоже что ничего хорошего в �?той на�?тройке нет
	//rfm69_write_reg(0x0A, 0x80);//start rc calibration
	rfm69_write_reg(0x12, 0x00);//pa ramp up time - �?тавим мак�?имальное, в �?том �?лучае RSSI был наилучший
	rfm69_setModeRx();

	//реинициализаци�? драйвера rscode
	//rscode_init_npar(&rscode_driver, cfg_current.fields.bytesForCodding_NPAR);
}

static uint8_t regBuf[0x71];
void readAllRFM69RegsToConsole(void)
{
	uint8_t mess[] = "Try to print all RFM69 regs:\n";
	HAL_UART_Transmit(&huart2, mess, sizeof(mess), 1000);
	uint8_t buffForMess[30];
	//console_print();
	for(uint8_t i = 0; i < 0x71; i++)
	{
		rfm69_read_reg(i, /*&regbuf*/&regBuf[i]);
		volatile uint8_t len = sprintf((char*)buffForMess, "RFM69 REG[%d] = %d\n", i, regBuf[i]);
		HAL_UART_Transmit(&huart2, buffForMess, len, 1000);
		//console_print("reg ", i, " = ", regBuf[i], "\n");
	}
	uint8_t mess1[] = "Print all RFM69 regs done\n";
	HAL_UART_Transmit(&huart2, mess1, sizeof(mess1), 1000);
	//console_print("Print all RFM69 regs done");

}
