#include "stm32l4xx_hal.h"
#include "PAC1934.h"

uint8_t PAC1934_WriteRegister (I2C_HandleTypeDef *hi2c, uint8_t address, uint8_t reg, uint8_t value)
{
	if (HAL_I2C_Mem_Write(hi2c,address<< 1,reg,1,&value,1,1000)==HAL_OK)
		return 0;
	return 1;
}

uint8_t PAC1934_ReadByte(I2C_HandleTypeDef *hi2c, uint8_t address, uint8_t reg, uint8_t *value)
{
    // Shift values to create properly formed integer
	uint8_t i2c_state;
	uint8_t buf[1] = {reg};
	while((i2c_state = HAL_I2C_Master_Transmit(hi2c, address << 1, buf, 1, 10)) == HAL_BUSY);
	if (i2c_state == HAL_ERROR)
		return 1;
	//HAL_Delay(5);
	while((i2c_state = HAL_I2C_Master_Receive(hi2c, address << 1, buf, 1, 10)) == HAL_BUSY);
	if (i2c_state == HAL_ERROR)
			return 1;

	*value = buf[0];
	return 0;
}

uint8_t PAC1934_ReadRegister(I2C_HandleTypeDef *hi2c, uint8_t address, uint8_t reg, uint16_t *value)
{
	uint8_t i2c_state;
	uint8_t buf[2] = {reg, 0};
	while((i2c_state = HAL_I2C_Master_Transmit(hi2c, address << 1, buf, 1, 10)) == HAL_BUSY);
	if (i2c_state == HAL_ERROR)
		return 1;
	//HAL_Delay(5);
	while((i2c_state = HAL_I2C_Master_Receive(hi2c, address << 1, buf, 2, 10)) == HAL_BUSY);
	if (i2c_state == HAL_ERROR)
		return 1;

	*value = (buf[0] << 8) + buf[1];
	return 0;
}

uint8_t PAC1934_Init(I2C_HandleTypeDef *hi2c)
{
	//HAL_GPIO_WritePin(PAC1934_SLOW_GPIO_Port, PAC1934_SLOW_Pin, GPIO_PIN_RESET);
	//HAL_GPIO_WritePin(PAC1934_PWRDN_GPIO_Port, PAC1934_PWRDN_Pin, GPIO_PIN_SET);

	HAL_Delay(10);

	uint8_t data = 0;
	data |= (	(1 << PAC1934_CTRL_SR0)
			+	(1 << PAC1934_CTRL_SR1)
			//+	(1 << PAC1934_CTRL_SING)
			);
	if (PAC1934_WriteRegister(hi2c, PAC1934_addr, PAC1934_CTRL, data))
		return 1;

	data = 0;

	data |= (	(1 << PAC1934_CHANNEL_DIS_NO_SKIP)
			//+	(1 << PAC1934_CHANNEL_DIS_CH4_OFF)
			);
	if (PAC1934_WriteRegister(hi2c, PAC1934_addr, PAC1934_CHANNEL_DIS, data))
		return 2;

	data = 0;

	if (PAC1934_WriteRegister(hi2c, PAC1934_addr, PAC1934_NEG_PWR, data))
		return 3;

	data = 0;

	if (PAC1934_WriteRegister(hi2c, PAC1934_addr, PAC1934_SLOW, data))
		return 4;

	if (PAC1934_Refresh(hi2c))
		return 5;

	return 0;
}

uint8_t PAC1934_Refresh(I2C_HandleTypeDef *hi2c)
{
	uint8_t i2c_state;
	uint8_t buf[1] = {PAC1934_REFRESH};
	while((i2c_state = HAL_I2C_Master_Transmit(hi2c, PAC1934_addr << 1, buf, 1, 10)) == HAL_BUSY);
	if (i2c_state == HAL_ERROR)
		return 1;
	return 0;
}

uint8_t PAC1934_RefreshV(I2C_HandleTypeDef *hi2c)
{
	uint8_t i2c_state;
	uint8_t buf[1] = {PAC1934_REFRESH_V};
	while((i2c_state = HAL_I2C_Master_Transmit(hi2c, PAC1934_addr << 1, buf, 1, 10)) == HAL_BUSY);
	if (i2c_state == HAL_ERROR)
		return 1;
	return 0;
}

uint8_t PAC1934_GetVoltage(I2C_HandleTypeDef *hi2c, uint8_t channel, float *volts)
{
	uint16_t data = 0;
	if (PAC1934_ReadRegister(hi2c, PAC1934_addr, channel, &data))
		return 1;
	*volts = (float) (32*data)/65536;
	return 0;
}

uint8_t PAC1934_GetCurrent(I2C_HandleTypeDef *hi2c, uint8_t channel, float *amps)
{
	uint16_t data = 0;
	if (PAC1934_ReadRegister(hi2c, PAC1934_addr, channel, &data))
		return 1;

	*amps = (float) (2000*data)/65536;//2000 = 100mV/Rsense; Rsense = 0.05 Ohm
	return 0;
}

uint8_t PAC1934_GetUI(I2C_HandleTypeDef *hi2c, uint8_t channel, uint16_t *volts, uint16_t *amps)
{
	uint16_t data = 0;
	if (PAC1934_ReadRegister(hi2c, PAC1934_addr, channel + PAC1934_VBUS1, &data))
		return 1;

	float tmp = (float) data*32/65536*1000;
	*volts = (uint16_t) tmp;

	data = 0;
	if (PAC1934_ReadRegister(hi2c, PAC1934_addr, channel + PAC1934_VSENSE1, &data))
		return 1;

	tmp = (float) (2000*data)/65536;//2000 = 100mV/Rsense; Rsense = 0.05 Ohm
	*amps = (uint16_t) tmp;
	return 0;
}

uint8_t PAC1934_GetUI_AVG(I2C_HandleTypeDef *hi2c, uint8_t channel, uint16_t *volts, uint16_t *amps)
{
	uint16_t data = 0;
	if (PAC1934_ReadRegister(hi2c, PAC1934_addr, channel + PAC1934_VBUS1_AVG, &data))
		return 1;

	float tmp = (float) data*32/65536*1000;
	*volts = (uint16_t) tmp;

	data = 0;
	if (PAC1934_ReadRegister(hi2c, PAC1934_addr, channel + PAC1934_VSENSE1_AVG, &data))
		return 1;

	tmp = (float) (2000*data)/65536;//2000 = 100mV/Rsense; Rsense = 0.05 Ohm
	*amps = (uint16_t) tmp;
	return 0;
}
