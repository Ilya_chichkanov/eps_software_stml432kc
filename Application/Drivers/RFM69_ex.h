/*
 * RFM69.h
 *
 *  Created on: Sep 18, 2020
 *      Author: Данил
 */

#ifndef INC_RFM69_H_
#define INC_RFM69_H_



#endif /* INC_RFM69_H_ */

#include "stm32l4xx_hal.h"


#include "rfm69.h"
#include "radio_config.h"

void rfm69_CS_set_LOW(void);
void rfm69_CS_set_HIGH(void);
void rfm69_reset_hard(void);
void UM_SPI1_transfer(uint16_t write, uint16_t *read);
void um_delay_us(uint32_t delay);
uint64_t rfm69_get_tick_us(void);
void updateRfm69Cfg();
void readAllRFM69RegsToConsole(void);
