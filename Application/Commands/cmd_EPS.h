/*
 * cmd_OBC.h
 *
 *  Created on: 17 ���. 2021 �.
 *      Author: Ilia
 */

#ifndef INC_CMD_OBC_H_
#define INC_CMD_OBC_H_
#include <stdint.h>
#include <stdbool.h>
#include "stm32l4xx_hal.h"
#include "repo_data_schema.h"
#include "main.h"
/*maassage id for input commands*/
#define CMD_GET_FIRMWARE_VERSION  0xFFE0
#define CMD_POWER_CHANNEL_ON      0xAB12
#define CMD_POWER_CHANNEL_OFF     0xAB13
/*maassage id for output commands*/
#define CMD_RESP_VERSION_SW       0xFFE1

int prob_command(uint8_t sender,uint8_t * data);
int prob_long_command(uint8_t sender,uint8_t *data);
int get_firmare_version (uint8_t sender,uint8_t *data);
int Pwr_sw_off(uint8_t sender,uint8_t * data);
int Pwr_sw_on(uint8_t sender,uint8_t *data);
#endif /* INC_CMD_OBC_H_ */
