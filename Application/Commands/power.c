/*
 * power.c
 *
 *  Created on: 15 апр. 2021 г.
 *      Author: Ilia
 */
#include "power.h"
#include "status.h"
int power_sw_on(uint8_t channel){
	struct status * dev_status = status_get();
	switch (channel){
	   case 1:
		   OBC_ON
		   dev_status->ch1 = 1;
	   case 2:
		   UHF_ON
		   dev_status->ch2 = 1;
	   case 3:
		   RFM69_ON
		   dev_status->ch_rfm=1;
	   default:
		   break;
	}
	return 0;
}

int power_sw_off(uint8_t channel){
	struct status * dev_status = status_get();
	switch (channel){
	   case 1:
		   OBC_OFF
		   dev_status->ch1 = 0;
	   case 2:
		   UHF_OFF//очень плохая команда,c земли наверно лучше такого не посылать (лучше убрать
		   dev_status->ch2 = 0;
	   case 3:
		   RFM69_OFF
		   dev_status->ch_rfm = 0;
	   default:
		   break;
	}

	return 0;
}
