/*
 * cmd_OBC.c
 *
 *  Created on: 17 ���. 2021 �.
 *      Author: Ilia
 */

#ifndef SRC_CMD_OBC_C_
#define SRC_CMD_OBC_C_
#include <cmd_EPS.h>
#include "unican.h"
#include "settings.h"
#include "board.h"
#include "status.h"
#include "power.h"
static const char * tag = "Cmd_EPC";

int Pwr_sw_on(uint8_t sender,uint8_t *data){
	if(sender==UNICAN_GROUND_ADDRESS){
		power_sw_on(data[0]);
	}
	return 0;
}

int Pwr_sw_off(uint8_t sender,uint8_t *data){
	if(sender==UNICAN_GROUND_ADDRESS){
		power_sw_off(data[0]);
	}
	return 0;
}
#endif /* SRC_CMD_OBC_C_ */
