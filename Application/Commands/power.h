#include "main.h"

typedef enum {CH1=1, CH2=2, CH3=3} channels;
typedef enum {ON=1, OFF=0} channel_states;

int power_sw_on(uint8_t channel);

int power_sw_off(uint8_t channel);
