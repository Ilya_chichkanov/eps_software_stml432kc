/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define DEBUG_LED_Pin GPIO_PIN_0
#define DEBUG_LED_GPIO_Port GPIOA
#define CH1_DIS_Pin GPIO_PIN_1
#define CH1_DIS_GPIO_Port GPIOA
#define CH2_DIS_Pin GPIO_PIN_3
#define CH2_DIS_GPIO_Port GPIOA
#define SPI1_NSS_Pin GPIO_PIN_4
#define SPI1_NSS_GPIO_Port GPIOA
#define EN3_Pin GPIO_PIN_0
#define EN3_GPIO_Port GPIOB
#define EN1_Pin GPIO_PIN_1
#define EN1_GPIO_Port GPIOB
#define CAN1_SELECT_Pin GPIO_PIN_8
#define CAN1_SELECT_GPIO_Port GPIOA
#define CAN2_SELECT_Pin GPIO_PIN_9
#define CAN2_SELECT_GPIO_Port GPIOA
#define RF_RESET_Pin GPIO_PIN_10
#define RF_RESET_GPIO_Port GPIOA
#define CH2_TH_Pin GPIO_PIN_3
#define CH2_TH_GPIO_Port GPIOB
#define EN2_Pin GPIO_PIN_4
#define EN2_GPIO_Port GPIOB
#define EN4_Pin GPIO_PIN_5
#define EN4_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define OBC_OFF HAL_GPIO_WritePin(EN1_GPIO_Port, EN1_Pin  , GPIO_PIN_SET);
#define OBC_ON HAL_GPIO_WritePin(EN1_GPIO_Port, EN1_Pin  , GPIO_PIN_RESET);
#define UHF_OFF HAL_GPIO_WritePin(EN2_GPIO_Port, EN2_Pin  , GPIO_PIN_SET);
#define UHF_ON HAL_GPIO_WritePin(EN2_GPIO_Port, EN2_Pin  , GPIO_PIN_RESET);
#define RFM69_OFF HAL_GPIO_WritePin(RF_RESET_GPIO_Port, RF_RESET_Pin, GPIO_PIN_SET);
#define RFM69_ON HAL_GPIO_WritePin(RF_RESET_GPIO_Port, RF_RESET_Pin, GPIO_PIN_RESET);
#define DEBUG_LED_ON HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_SET);
#define DEBUG_LED_OFF HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_RESET);
#define DEBUG_TOGGLE HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
