/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "log_utils.h"
#include "repo_cmd.h"
#include "ring_buffer.h"
#include "unican.h"
#include "settings.h"
#include "status.h"
#include "telemerty.h"
#include "log_utils.h"
#include "default_settings.h"
#include "power.h"
#include "board.h"
#include "errors.h"
#include "logbook.h"
#include "iwdg.h"
#include "../../Application/Drivers/RFM69_ex.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
SemaphoreHandle_t I2C_mtx;
StaticSemaphore_t I2C_mtx_buffer;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
uint32_t heatbeatCounter;
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
osThreadId defaultTaskHandle;
uint32_t defaultTaskBuffer[ 128 ];
osStaticThreadDef_t defaultTaskControlBlock;
osThreadId dispatcherTaskHandle;
uint32_t dispatcherTaskBuffer[ 512 ];
osStaticThreadDef_t dispatcherTaskControlBlock;
osThreadId telemetryTaskHandle;
uint32_t telemetryTaskBuffer[ 512 ];
osStaticThreadDef_t telemetryTaskControlBlock;
osThreadId housekeepingHandle;
uint32_t housekeepingBuffer[ 128 ];
osStaticThreadDef_t housekeepingControlBlock;
osThreadId plannerTaskHandle;
uint32_t plannerTaskBuffer[ 256 ];
osStaticThreadDef_t plannerTaskControlBlock;
osThreadId RFM_taskHandle;
uint32_t RFM_taskBuffer[ 256 ];
osStaticThreadDef_t RFM_taskControlBlock;
osTimerId heatbeatTimerHandle;
osStaticTimerDef_t heatbeatTimerControlBlock;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void const * argument);
void StartDispatcherTask(void const * argument);
void StartTelemetryTask(void const * argument);
void StartHousekeepingTask(void const * argument);
void StartPlannerTask(void const * argument);
void RFM_taskStart(void const * argument);
void heatbeatTimerCallback(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* GetTimerTaskMemory prototype (linked to static allocation support) */
void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize );

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/* USER CODE BEGIN GET_TIMER_TASK_MEMORY */
static StaticTask_t xTimerTaskTCBBuffer;
static StackType_t xTimerStack[configTIMER_TASK_STACK_DEPTH];

void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize )
{
  *ppxTimerTaskTCBBuffer = &xTimerTaskTCBBuffer;
  *ppxTimerTaskStackBuffer = &xTimerStack[0];
  *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
  /* place for user code */
}
/* USER CODE END GET_TIMER_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
	I2C_mtx = xSemaphoreCreateMutexStatic(&I2C_mtx_buffer);
	xSemaphoreGive(I2C_mtx);

  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* Create the timer(s) */
  /* definition and creation of heatbeatTimer */
  osTimerStaticDef(heatbeatTimer, heatbeatTimerCallback, &heatbeatTimerControlBlock);
  heatbeatTimerHandle = osTimerCreate(osTimer(heatbeatTimer), osTimerPeriodic, NULL);

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadStaticDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128, defaultTaskBuffer, &defaultTaskControlBlock);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of dispatcherTask */
  osThreadStaticDef(dispatcherTask, StartDispatcherTask, osPriorityNormal, 0, 512, dispatcherTaskBuffer, &dispatcherTaskControlBlock);
  dispatcherTaskHandle = osThreadCreate(osThread(dispatcherTask), NULL);

  /* definition and creation of telemetryTask */
  osThreadStaticDef(telemetryTask, StartTelemetryTask, osPriorityNormal, 0, 512, telemetryTaskBuffer, &telemetryTaskControlBlock);
  telemetryTaskHandle = osThreadCreate(osThread(telemetryTask), NULL);

  /* definition and creation of housekeeping */
  osThreadStaticDef(housekeeping, StartHousekeepingTask, osPriorityNormal, 0, 128, housekeepingBuffer, &housekeepingControlBlock);
  housekeepingHandle = osThreadCreate(osThread(housekeeping), NULL);

  /* definition and creation of plannerTask */
  osThreadStaticDef(plannerTask, StartPlannerTask, osPriorityNormal, 0, 256, plannerTaskBuffer, &plannerTaskControlBlock);
  plannerTaskHandle = osThreadCreate(osThread(plannerTask), NULL);

  /* definition and creation of RFM_task */
  osThreadStaticDef(RFM_task, RFM_taskStart, osPriorityNormal, 0, 256, RFM_taskBuffer, &RFM_taskControlBlock);
  RFM_taskHandle = osThreadCreate(osThread(RFM_task), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  heatbeatCounter=0;
  osTimerStart(heatbeatTimerHandle, DEFAULT_HEATBEAT_PEROOD);
  struct status* dev_status = status_get();
  dev_status->state = EPS_UHF;
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
	const char * tag = "DefaultTask EPC";
	LOGI(tag,"Start");
	const TickType_t xFrequency = 2000;
	TickType_t xLastWakeTime = xTaskGetTickCount();
	for(;;)
	{
/*		unican_send_cmd(&msg);
		if(unican_resp==OK){
			LOGI(tag,"COMMAND DONE");
		}
		else if(unican_resp==ERROR){
			LOGI(tag,"COMMAND ERROR");
		}
		else{
			LOGI(tag,"COMMAND TOMEOUT");
		}*/
		//unican_send(&msg);
		DEBUG_TOGGLE
		vTaskDelayUntil( &xLastWakeTime, xFrequency );
	}
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_StartDispatcherTask */
struct unican_handler command_dispatcher;
/**
* @brief Function implementing the dispatcherTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartDispatcherTask */
void StartDispatcherTask(void const * argument)
{
  /* USER CODE BEGIN StartDispatcherTask */
  /* Infinite loop */
	static const char *tag = "EPC dispatcher task";
	LOGI(tag,"Start");
	unican_add_handler(UNICAN_HANDLERID_GROUND, &command_dispatcher);
	struct cmd_t cmd_new;
	uint8_t cmd_table_id;
	for(;;)
	{
		if (unican_receive(&command_dispatcher, portMAX_DELAY)) {
			if(command_dispatcher.packet.msg_id==UNICAN_HEATBEAT){
				heatbeatCounter = 0;
			}
			else{
				int16_t res_check = cmd_check(command_dispatcher.packet.msg_id,command_dispatcher.packet.data_len,&cmd_table_id);
				if(res_check==CMD_OK){
					cmd_new.id_cmd = cmd_table_id;
					cmd_new.sender = command_dispatcher.packet.sender;
					cmd_new.args   = (command_dispatcher.packet.data_len>0)?command_dispatcher.packet.data:NULL;
					uint16_t cmd_res = cmd_run(cmd_new.id_cmd,cmd_new.sender, cmd_new.args);
					if(cmd_res==0){
						unican_ack(cmd_new.sender,command_dispatcher.packet.msg_id);
					}
					else{
						unican_nack(cmd_new.sender,command_dispatcher.packet.msg_id,cmd_res);
					}
				}

				else if(res_check==CMD_NOT_FOUND){
					unican_nack(command_dispatcher.packet.sender,command_dispatcher.packet.msg_id,ERR_ANKNOWN_CMD);
					LOGI(tag,"UNKNOWN COMMAND");
				}
				else{
					unican_nack(command_dispatcher.packet.sender,command_dispatcher.packet.msg_id,ERR_CMD_WRONG_PARAM);
					LOGI(tag,"WRONG PARAM");
				}
			}
		}
	}

  /* USER CODE END StartDispatcherTask */
}

/* USER CODE BEGIN Header_StartTelemetryTask */
/**
* @brief Function implementing the telemetryTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTelemetryTask */
void StartTelemetryTask(void const * argument)
{
  /* USER CODE BEGIN StartTelemetryTask */
  /* Infinite loop */
	static const char *tag = "EPS telemerty";
	const TickType_t xFrequency = DEFAULT_TEL_REG_PERIOD;
	TickType_t xLastWakeTime;
	LOGI(tag,"START");
	xLastWakeTime = xTaskGetTickCount();
	for(;;)
	{
		if (xSemaphoreTake(I2C_mtx, 100)== pdTRUE){
		  telemerty_regylar();
		  xSemaphoreGive(I2C_mtx);
		  vTaskDelayUntil( &xLastWakeTime, xFrequency );
		}
	}

  /* USER CODE END StartTelemetryTask */
}

/* USER CODE BEGIN Header_StartHousekeepingTask */
/**
* @brief Function implementing the housekeeping thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartHousekeepingTask */
void StartHousekeepingTask(void const * argument)
{
  /* USER CODE BEGIN StartHousekeepingTask */
  /* Infinite loop */
	static const char *tag = "EPS houseeping task";
	const TickType_t xFrequency = DEFAULT_HEATBEAT_PEROOD;
	TickType_t xLastWakeTime= xTaskGetTickCount();
	LOGI(tag, "START");
	struct status* dev_status = status_get();
	uint8_t sens_err[SENS_EPC_COUNT] = {0};
	for(;;)
	{
	  HAL_IWDG_Refresh(&hiwdg);
	  if(heatbeatCounter>MAX_HEATBEAT_FAIL){
		  if(dev_status->ch1==ON){
			  LOGI(tag, "OBC DOES NOT WORK.TURN OFF CH1");
			  //power_sw_off(CH1);
		  }
		  /*DO SMTH*/
	  }

	  for (uint8_t i=0;i<SENS_EPC_COUNT;i++){
		  if(dev_status->sens_err[i]!=sens_err[i]){
			  struct Logbook_t logbook;
			  logbook.error = dev_status->sens_err[i];
			  logbook_write(&logbook);
		  }
		  sens_err[i] = dev_status->sens_err[i];
	  }

	  vTaskDelayUntil( &xLastWakeTime, xFrequency );
	}
  /* USER CODE END StartHousekeepingTask */
}

/* USER CODE BEGIN Header_StartPlannerTask */
/**
* @brief Function implementing the plannerTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartPlannerTask */
void StartPlannerTask(void const * argument)
{
  /* USER CODE BEGIN StartPlannerTask */
  /* Infinite loop */
	static const char *tag = "EPS planner task";
	const TickType_t xFrequency = 2000;
	TickType_t xLastWakeTime= xTaskGetTickCount();
	struct status* dev_status = status_get();
	LOGI(tag, "START");
	for(;;)
	{
	  switch (dev_status->state)
	   {
	   case EPS_UHF:
			LOGI(tag, "EPS+UHF");
			if(dev_status->ch1 == ON){
				dev_status->state = EPS_UHF_OBC;
				//vTaskSuspend(telemetryTaskHandle);
			}
		   break;
	   case EPS_UHF_OBC:
		   LOGI(tag, "EPS+UHF+OBC");
			if(dev_status->ch1 == OFF){
				dev_status->state = EPS_UHF;
				//vTaskResume(telemetryTaskHandle);
			}
	   default:
		   break;
	   }

	  vTaskDelayUntil( &xLastWakeTime, xFrequency );
	}
  /* USER CODE END StartPlannerTask */
}

/* USER CODE BEGIN Header_RFM_taskStart */
/**
* @brief Function implementing the RFM_task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_RFM_taskStart */
void RFM_taskStart(void const * argument)
{
  /* USER CODE BEGIN RFM_taskStart */
  /* Infinite loop */
	static const char *tag = "EPS RFM69 task";
	const TickType_t xFrequency = 2000;
	TickType_t xLastWakeTime= xTaskGetTickCount();
	LOGI(tag, "START");
	for(;;)
	{
		if (xSemaphoreTake(I2C_mtx, 100)== pdTRUE){
			struct telemerty_reg * tel_reg = telemerty_regular_get();

			char tmp[100];
			for (uint8_t i = 0; i <= (UI_SENSORS_EPS_LAST - UI_SENSORS_EPS_FIRST); i++)
			{
			  char tmp[112];
			  uint16_t voltage =  tel_reg->voltage_param[i];
			  uint16_t current =  tel_reg->current_param[i];
			  sprintf(tmp, "ID %2d %5d.%3d V %d mA\n\r", i, voltage/1000,voltage - (voltage/1000)*1000, current);
			  rfm69_send((uint8_t *)tmp, cfg_current.fields.rf_packet_length);
			}

			for (uint8_t i = 0; i <= (T_SENS_EPS_LAST - T_SENS_EPS_FIRST); i++)
			{
			  char tmp[112];
			  sprintf(tmp, "ID %2d %5d\n\r", i + T_SENS_FIRST, tel_reg->temperature_param[i]);
			  rfm69_send((uint8_t *)tmp, cfg_current.fields.rf_packet_length);
			}

			LOGI(tag,"SENDING");
			xSemaphoreGive(I2C_mtx);
			vTaskDelayUntil( &xLastWakeTime, xFrequency );
		}
	}
  /* USER CODE END RFM_taskStart */
}

/* heatbeatTimerCallback function */
void heatbeatTimerCallback(void const * argument)
{
  /* USER CODE BEGIN heatbeatTimerCallback */
	heatbeatCounter+=1;
  /* USER CODE END heatbeatTimerCallback */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
